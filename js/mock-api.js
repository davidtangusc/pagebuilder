app.run(function($httpBackend) {
	$httpBackend
		.whenGET('/pages')
		.respond(200, [
			{ id: 1, title: 'Spring 2014' },
			{ id: 2, title: 'Fall 2014' },
			{ id: 3, title: 'Winter 2014' },
			{ id: 4, title: 'New Years 2015' },
			{ id: 5, title: 'Back to School Sale' }
		]);

	$httpBackend
		.whenGET(/\/pages\/\d/)
		.respond(200, {
			rows: [
				{ 
					id: 1,
					droplets: [
						{ title: 'Droplet 1' },
						{ title: 'Droplet 2' }
					] 
				},
				{ 
					id: 2,
					droplets: [
						{ title: 'Droplet 1' },
						{ title: 'Droplet 2' },
						{ title: 'Droplet 3' }
					] 
				}
			]
		});

	$httpBackend.whenGET(/^\/js\/templates\//).passThrough();
});