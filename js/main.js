var app = angular.module('pagebuilder', ['ngRoute', 'ngMockE2E', 'ngSanitize']);

app.config(function($routeProvider) {
	$routeProvider
		.when('/pages', {
			templateUrl: '/js/templates/index.html',
			controller: 'PagesController',
			controllerAs: 'vm'
		})
		.when('/pages/:id/edit', {
			templateUrl: '/js/templates/edit-page.html',
			controller: 'EditPageController',
			controllerAs: 'vm'
		})
		.otherwise({
			redirectTo: '/pages'
		})
});

app.controller('PagesController', function(page, $scope) {
	var vm = this;

	page.all().then(function(pageData) {
		vm.pages = pageData;
	});

	return vm;
});

app.controller('EditPageController', function($routeParams, page, $scope, state) {
	var vm = this;

	vm.dropletform = {
		title: 'some title',
		value: '<strong>Test</strong>'
	};

	page.find($routeParams.id).then(function(page) {
		vm.page = page;
	});

	$scope.$watch("vm.page.rows", function(value) {
		if (value instanceof Array) {
			console.log("Model: " + value.map(function(e){return e.id}).join(','));
		}
  }, true);

	vm.addDroplet = function(row) {
		console.log(row);
		state.rowInEditMode = row;
	};

	vm.applyDropletToRow = function() {
		console.log(vm.dropletform.title, vm.dropletform.value);

		state.rowInEditMode.droplets.push({
			title: vm.dropletform.title,
			value: vm.dropletform.value
		});
	};

	return vm;
});

app.factory('page', function($http) {

	function all() {
		return $http.get('/pages').then(function(response) {
			return response.data;
		});
	}

	function find(id) {
		return $http.get('/pages/' + id).then(function(response) {
			return response.data;
		});
	}

	return {
		all: all,
		find: find
	};
});

app.value('state', {});

app.directive('dndList', function() {
	return {
		restrict: 'A',
		scope: {
			models: '='
		},
		link: function($scope, $el, attr) {
			var startIndex;
			var toUpdate;

			$el.sortable({
				start: function(event, ui) {
					startIndex = $(ui.item).index();
				},

				stop: function(event, ui) {
					var newIndex = $(ui.item).index();
					var draggedModel = $scope.models[startIndex];

					$scope.$apply(function() {
						$scope.models.splice(startIndex, 1);
						$scope.models.splice(newIndex, 0, draggedModel);
						console.log($scope.models);
					});
				}
			});
		}
	}
});