<!DOCTYPE html>
<html ng-app="pagebuilder">
<head>
	<title>Page Builder</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="css/main.css">
</head>
<body>

<div class="container">
	<div ng-view></div>	
</div>


<script src="bower_components/jquery/dist/jquery.js"></script>
<script src="https://code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
<script src="bower_components/angular/angular.js"></script>
<script src="bower_components/angular-route/angular-route.js"></script>
<script src="bower_components/angular-mocks/angular-mocks.js"></script>
<script src="bower_components/angular-sanitize/angular-sanitize.js"></script>
<script src="js/main.js"></script>
<script src="js/mock-api.js"></script>

</body>
</html>